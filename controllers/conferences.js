exports.getAllconferences = (req, res, next) => {
    const conferences = [
        {
            name: 'Laravel',
            place: 'Amsterdam'
        },
        {
            name: 'Nodejs',
            place: 'New York'
        },
        {
            name: 'Sequelize',
            place: 'Barcelona'
        }
    ];
    res.render('conference/index', {
        path: '/conference',
        title: 'All conferences',
        description: 'What conferences are upcoming?',
        conferences: conferences
    });
};

exports.getAddConference = (req, res, next) => {
    res.render('conference/addConference', {
        path: '/add-conference',
        title: 'Add conference',
        description: 'What kind of conferences do you want to register?'
    });
};

exports.postStoreConference = (req, res, next) => {
    res.redirect('/conference');
};