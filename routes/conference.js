const path = require('path');

const express = require('express');
const conferencesRoutes = require('../controllers/conferences');
const router = express.Router();

//allConferences
router.get('/', conferencesRoutes.getAllconferences);

//AddUser
router.get('/add-conference', conferencesRoutes.getAddConference);
router.post('/add-conference', conferencesRoutes.postStoreConference);

module.exports = router;