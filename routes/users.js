const path = require('path');

const express = require('express');
// LES 05
const usersController = require('../controllers/users');

const router = express.Router();

//allUsers
router.get('/',usersController.getAllUsers);
//AddUser
router.get('/add-user', usersController.getAddUsers);


//router.addUser = ();

module.exports = router;